package br.ufsc.labsec.desafio;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test class for br.ufsc.labsec.desafio.Login.
 */
public class TestLogin {
	
	private Login login = new Login();
		
	@Test
	public void testInitialState() {
		Login freshInstance = new Login();
		assertEquals("", freshInstance.getUser());
		assertEquals("", freshInstance.getPassword());
		assertEquals("", freshInstance.getMessage());
	}
	
	@Test
	public void testSuccessfulRegistration() {
		login.clearAll();
		login.setUser("user1");
		login.setPassword("pass1");
		assertEquals("user1", login.getUser());
		assertEquals("pass1", login.getPassword());
		
		login.register();
		assertEquals("User registered!", login.getMessage());
	}
	
	@Test
	public void testDuplicatedUserRegistration() {
		login.clearAll();
		login.setUser("user2");
		login.setPassword("pass2");
		login.register();
		
		login.setPassword("extremelyGoodPassword");
		login.register();
		assertEquals("Username already exists.", login.getMessage());
	}
	
	@Test
	public void testSuccessfulLogin() {
		login.clearAll();
		login.setUser("user3");
		login.setPassword("pass3");
		login.register();

		assertEquals("user3", login.getUser());
		assertEquals("pass3", login.getPassword());

		login.login();
		assertEquals("Login ok.", login.getMessage());
	}
	
	@Test
	public void testIncorrectPasswordLogin() {
		login.clearAll();
		login.setUser("user4");
		login.setPassword("pass4");
		login.register();
		
		login.setPassword("pass3");
		login.login();
		assertEquals("Login fail.", login.getMessage());
	}
	
	@Test
	public void testNoSuchUserLogin() {
		login.clearAll();
		login.setUser("user99");
		login.setPassword("pass99");
		login.login();
		assertEquals("No such user.", login.getMessage());
	}
}
