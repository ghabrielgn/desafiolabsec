package br.ufsc.labsec.desafio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Test class for br.ufsc.labsec.desafio.HashManager.
 */
public class TestHashManager {

	private HashManager manager = new HashManager();
		
	@Test
	public void testSaltSetter() {
		manager.setDefaultSalt("abcd".getBytes());
		assertEquals("abcd", new String(manager.getDefaultSalt()));
	}
	
	@Test
	public void testHashing() {
		manager.newDefaultSalt();
		String pass = "best password ever";
		assertTrue(manager.verify(pass, manager.hash(pass)));
		assertFalse(manager.verify("not the best password ever", manager.hash(pass)));
		assertFalse(pass.equals(manager.hash(pass)));
	}
}
