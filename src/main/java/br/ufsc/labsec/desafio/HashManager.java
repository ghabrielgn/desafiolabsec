package br.ufsc.labsec.desafio;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Manages the hashing and verification of passwords.
 */
public class HashManager {
	/**
	 * The number of iterations made by the hashing function.
	 * A high amount implies a better password, but a slower generation.
	 */
	private static final int ITERATIONS = 1000;

	/**
	 * The length of the key.
	 */
	private static final int KEY_LENGTH = 192;
	
	/**
	 * Used to generate salts.
	 */
	private static final Random SALT_GENERATOR = new SecureRandom();
	
	/**
	 * The default salt.
	 */
	private byte[] defaultSalt = new byte[16];
	
	public HashManager() {
		newDefaultSalt();
	}
	
	/**
	 * Generates a new salt and uses it as the default salt.
	 */
	public void newDefaultSalt() {
		SALT_GENERATOR.nextBytes(defaultSalt);
	}
	
	/**
	 * Changes the default salt.
	 */
	public void setDefaultSalt(byte[] salt) {
		defaultSalt = salt;
	}
	
	/**
	 * @return The default salt being used.
	 */
	public byte[] getDefaultSalt() {
		return defaultSalt;
	}	
	
	/**
	 * Hashes a password using the default salt.
	 * @param password The password to be hashed
	 * @return A secure hash of the password
	 */
	public String hash(String password) {
		return hash(password, defaultSalt);
	}
	
	/**
	 * Hashes a password using a user-supplied salt.
	 * @param password The password to be hashed
	 * @param salt The salt to be used
	 * @return A secure hash of the password
	 */
	public String hash(String password, byte[] salt) {
		char[] passChars = password.toCharArray();
		PBEKeySpec spec = new PBEKeySpec(passChars, salt, ITERATIONS, KEY_LENGTH);
		SecretKeyFactory key = null;
		byte[] result = null;
		try {
			key = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");			
			result = key.generateSecret(spec).getEncoded();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return String.format("%x", new BigInteger(result));
	}
	
	/**
	 * Verifies if a password attempt matches the correct password.
	 * @param attempt The password entered by the user
	 * @param password The correct, hashed password
	 * @param salt The salt used in the hashing
	 * @return true if it's the correct password, false otherwise
	 */
	public boolean verify(String attempt, String password, byte[] salt) {
		return password.equals(hash(attempt, salt));
	}
	
	/**
	 * Verifies if a password attempt matches the correct password
	 * using the default salt.
	 * @param attempt The password entered by the user
	 * @param password The correct, hashed password
	 * @return true if it's the correct password, false otherwise
	 */
	public boolean verify(String attempt, String password) {
		return verify(attempt, password, defaultSalt);
	}
}
