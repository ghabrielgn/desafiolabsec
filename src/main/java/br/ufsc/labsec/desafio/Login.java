package br.ufsc.labsec.desafio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * Class for registering and authenticating users.
 */
@ManagedBean
@SessionScoped
public class Login {
	private static Map<String, String[]> users = new HashMap<>();

	private static final String DATABASE_PATH = "/database.txt";
	private Writer writer;
	private HashManager hashManager = new HashManager();
	
	private String message = "";
	private String user = "";
	private String password = "";
	
	/**
	 * @param overwrite true if the database should be cleared, false otherwise
	 * @return an output stream to the database
	 */
	private OutputStream getOutputStream(boolean overwrite) {
		URL resourceUrl = getClass().getResource(DATABASE_PATH);
		File file = null;
		OutputStream outputStream = null;
		try {
			file = new File(resourceUrl.toURI());
			outputStream = new FileOutputStream(file, !overwrite);			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return outputStream;
	}

	/**
	 * @return an output stream to the database (no data is lost)
	 */
	private OutputStream getOutputStream() {
		return getOutputStream(false);
	}

	/**
	 * Creates the database file if it doesn't exist yet.
	 */
	private void createDatabaseFile() {
		URL url = getClass().getResource("/");
		File path = null;
		try {
			path = new File(url.toURI()).getAbsoluteFile();
		} catch(URISyntaxException e) {
			e.printStackTrace();
		}
		
		File creator = new File(path + DATABASE_PATH);
		try {
			creator.createNewFile();			
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fills the map of users and prepares a file writer
	 */
	public Login() {
	    // Reads the database and fills the map of users.
		// Note that each register consists of three lines: one for the username,
		// one for the password and one for the salt.
		createDatabaseFile();
		InputStream inputStream = getClass().getResourceAsStream(DATABASE_PATH);
		
		InputStreamReader streamReader = new InputStreamReader(inputStream);
		try (BufferedReader reader = new BufferedReader(streamReader)) {
			String username;
			while ((username = reader.readLine()) != null) {
				String password = reader.readLine();
				String salt = reader.readLine();
				String[] data = {password, salt};
				users.put(username, data);
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		OutputStream outStream = getOutputStream();
		
		// Builds the BufferedWriter instance.
		try {
			writer = new BufferedWriter(new OutputStreamWriter(outStream, "utf-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return the current user name
	 */
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the current password
	 */
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return a message for the user
	 */
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Verifies if the current user and password match a user in the database.
	 */
	public void login() {
		if (!users.containsKey(user)) {
			message = "No such user.";
		} else {
			String[] data = users.get(user);
			String pass = data[0];
			byte[] salt = data[1].getBytes();
			if (hashManager.verify(password, pass, salt)) {
				message = "Login ok.";
			} else {
				message = "Login fail.";
			}
		}
	}

	/**
	 * Registers the current user and password.
	 */
	public void register() {
		if (users.containsKey(user)) {
			message = "Username already exists.";
			return;
		}
		
		// Replaces line breaks with underlines
		hashManager.newDefaultSalt();
		byte[] genSalt = hashManager.getDefaultSalt();
		for (int i = 0; i < genSalt.length; i++) {
			if (genSalt[i] == (byte)'\n') {
				genSalt[i] = (byte)'_';
			}
		}
		String salt = new String(hashManager.getDefaultSalt());

		// Hashes the password and puts it in the users map
		String hashedPassword = hashManager.hash(password, salt.getBytes());
		String[] data = {hashedPassword, salt};
		users.put(user, data);

		message = "User registered!";
		try {
			writer.append(user + "\n" + hashedPassword + "\n" + salt + "\n");
			writer.flush();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Clears the database
	 */
	public void clearAll() {
		getOutputStream(true);
		users.clear();
	}
}
